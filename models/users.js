const mongoose = require ("mongoose");
const usersSchema= new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "first name is required"]
    },
    lastName: {
        type: String,
        required: [true, "last name is required"]
    },
    email: {
        type: String,
        required: [true, "email is required"]
    },
    mobileNo: {
        type: String,
        required: [true, "username is required"]
    },
    password: {
        type: String,
        required: [true, "password name is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orders: [
        {
            productId: {type: String},
            productName: {type: String},
            orderAmount: {type: Number},
            orderPrice: {type: Number},
            dateOrdered: {type: Date, default: new Date()},
            status: {type: String, default: "order placed"},
        }
    ]
})

module.exports = mongoose.model("users", usersSchema);