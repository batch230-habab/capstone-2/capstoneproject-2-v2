const express = require("express");
const router = express.Router();
const productsControllers = require("../controllers/productsControllers");
const auth = require("../auth.js");

router.post("/newProduct", auth.verify, productsControllers.addProduct);

router.get("/availableProducts", productsControllers.getAllActive);

router.get("/:productId", productsControllers.getProduct);

router.post("/:productId/update", productsControllers.updateProduct);

router.patch("/:productId/archive", productsControllers.archiveProduct);

router.post("/:productId/clearOrders", productsControllers.clearOrders);

router.patch("/:productId/restockProduct", productsControllers.restockProduct);

router.get("/:productId/listOrders", productsControllers.listOrders);

router.get("/", productsControllers.AllProducts);

module.exports = router;