const express = require("express");
const router = express.Router();
const usersControllers = require("../controllers/usersControllers");
const productsControllers = require("../controllers/productsControllers");
const auth = require("../auth")

router.post("/register", usersControllers.registerUsersController);

router.post("/login", usersControllers.loginUser);

router.get("/userDetails", auth.verify, usersControllers.getUserDetails);

router.post("/addToCart", auth.verify, usersControllers.addToCart);

router.post("/finalCheckout", auth.verify, usersControllers.finalCheckout);
module.exports = router;

router.patch("/:userId/setToAdmin", auth.verify, usersControllers.setUserToAdmin);

router.get("/checkOrders", auth.verify, usersControllers.retrieveUserOrder);

router.get("/cart", auth.verify, usersControllers.retrieveUserOrder2);

router.delete("/:userId/deleteUser", auth.verify, usersControllers.deleteUser);

router.patch("/:userId/removeAdmin", auth.verify, usersControllers.removeAdmin)

router.get("/", usersControllers.getAllUsers);