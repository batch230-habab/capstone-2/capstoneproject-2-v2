const jwt = require("jsonwebtoken");
const secret = "morismementosAPI";

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    return jwt.sign(data, secret, {})
}

//? token verifier
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization
    if(typeof token !== "undefined"){
        console.log(token);
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (error, data) =>{
            if(error){
                res.send(err=>console.log(err));
            }else{
                next();
            }
        })
    }else{
        res.send(console.log('auth fail'));
    }
}

//? token decoder
module.exports.decode = (token) => {
    if(typeof token !== "undefined"){
        token = token.slice(7, token.length);
    }
    return jwt.verify(token, secret, (error, data)=> {
        if(error){
            return null;
        }else{
            return jwt.decode(token,{complete:true}).payload;
        }
    })
}