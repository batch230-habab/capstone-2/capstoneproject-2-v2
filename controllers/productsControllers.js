const mongoose = require('mongoose');
const products = require('../models/products.js');
const bcrypt  = require('bcrypt');
const auth = require ('../auth.js');

module.exports.addProduct = (req, res) =>{
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
    if(adminCheck == true){
        let newProduct = new products({
            prodName: req.body.prodName,
            prodDesc: req.body.prodDesc,
            price: req.body.price,
            stockAmount: req.body.stockAmount
        })
        newProduct.save()
        .then(result => res.send(true))
        .catch(error => res.send(false));
    }else{
        res.send(false);
    }
}

module.exports.getAllActive = (req, res) =>{
    products.find({isAvailable: true})
    .then(result => res.send(result))
    .catch(error => res.send(error));
}

module.exports.getProduct = (req, res) => {
    products.findById(req.params.productId)
    .then(result => res.send(result))
    .catch(error => res.send(error))
}


module.exports.updateProduct = (req, res) =>{
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
    if(adminCheck == true){
        products.findByIdAndUpdate(req.params.productId, {
            prodName: req.body.inputProdName,
            prodDesc: req.body.inputProdDesc,
            price: req.body.inputPrice
        }, {new: true})
        .then(res.send(true))
        .catch(error => res.send(error));
    }else{
        res.send(false)
    }
}

module.exports.archiveProduct = (req, res) => {

    products.findById(req.params.productId)
    .then(product => {
    products.findByIdAndUpdate(req.params.productId, {
        isAvailable: !product.isAvailable
    }, {new: true})
    .then(res.send(true))
})
}

module.exports.clearOrders = async (req, res) => {
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
    if(adminCheck ==  true){
        await products.findById(req.params.productId)
        .then(result =>{
            console.log(`clearing orders of ${result.prodName}`)
            result.orders.splice(0, result.orders.length);
            console.log(`${result.prodName}'s orders has been cleared`)
            result.save();
            res.send(`orders have been cleared: ${result}`)
        })
    }else{
    res.send(`User must be admin`)
    }
}

module.exports.restockProduct = (req,res) =>{
    products.findById(req.params.productId)
    .then(result =>{
        let x = parseInt(req.body.inputStockAmount)
        console.log(`adding stocks`)
        result.stockAmount += x
        result.save();
        res.send(true)
    })
}

module.exports.listOrders = (req, res) =>{
    const adminCheck = auth.decode(req.headers.authorization).isAdmin
    if(adminCheck == true){
        products.findById(req.params.productId)
        .then(result =>{
            let perUserId = result.orders.map(element => element.userId)
            let distinctUserId = [...new Set(perUserId)]
            let concatOrderTakers = "";
            let orderAmount = 0;
            let orderAmountTotal = 0;
            distinctUserId.forEach(userId =>{
                for(let i = 0; i < perUserId.length; i++){
                    if(perUserId[i] === userId){
                        orderAmount ++;
                        orderAmountTotal ++;
                    }
                }
                concatOrderTakers += `User ${userId.toString()} ordered ${orderAmount}\n<br>`
                orderAmount = 0;
                console.log(`${concatOrderTakers}`)
            })
            res.send(`${concatOrderTakers}\n <br>total of ${orderAmountTotal} orders for this item`);
        })
    }else{
        res.send(`User must be admin`)
    }
}

module.exports.AllProducts = (req, res) =>{
    products.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error));
}