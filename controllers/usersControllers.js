const users = require("../models/users.js");
const bcrypt = require('bcrypt');
const auth = require("../auth.js");
const products = require("../models/products");
const { forEach, result } = require('lodash');
const { distinct } = require("../models/users.js");

module.exports.registerUsersController = (req, res) =>{
    let successReg = false
    let newUser = new users({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        mobileNo: req.body.mobileNo,
        password: bcrypt.hashSync(req.body.password, 10),
        isAdmin: req.body.isAdmin
    })
    users.find().or([{email: req.body.email},{mobileNo:req.body.mobileNo}])
    .then(result =>{
        if(result.length != 0){
        }else{
            newUser.save();
            console.log(newUser);
            successReg = true;
        }
        res.send(successReg);
    })
    .catch(error=>res.send(error))
}

module.exports.loginUser = (req, res) => {
    users.findOne({email:req.body.email})
    .then(result =>{
        if (result == null){
            console.log("login failed");
            res.send(false);
        }else{
            const isPassCorrect = bcrypt.compareSync(req.body.password, result.password);
            if(isPassCorrect){
                res.send({access: auth.createAccessToken(result)});
            }else{
                res.send(false);
            }
        }
    })
    .catch(error => res.send(error));
}

module.exports.getUserDetails = (req, res) => {
    users.findById(auth.decode(req.headers.authorization).id)
    .then(result =>{
        result.password = "*********";
        res.send(result);
    })
    .catch(error => res.send(error))
}

module.exports.addToCart = async(req, res) =>{
    const userData = auth.decode(req.headers.authorization);
    let productToCart = await products.findById(req.body.productId).then(result => result);
    let receiptData = {
        userId: userData.id,
        email: userData.email,
        productId: req.body.productId,
        prodName: productToCart.prodName,
        stockAmount: productToCart.stockAmount,
        prodPrice: productToCart.price
        /* firstName: userData.firstName, */
    }
    console.log(`preparing ${productToCart.prodName}`)
    let isUserUpdated = await users.findById(receiptData.userId)
    .then(result => {
        if(receiptData.stockAmount > 0){
            result.orders.push({
                productName: receiptData.prodName,
                productId: receiptData.productId,
                prodName: receiptData.prodName,
                orderPrice: receiptData.prodPrice
            })
            return result.save()
            .then(saveResult => {
                console.log(saveResult);
                return true;
            })
            .catch(err=> {console.log(err);
            return false;})
        }else{
            console.log(`out of stock`);
            return false;
        }
    })
    console.log(`Your order`)
    let isProdUpdated = await products.findById(receiptData.productId)
    .then(prod => {
        prod.orders.push({
            userId: receiptData.userId,
            email: receiptData.email
        })
        if(prod.stockAmount >=1){
            prod.stockAmount -=1
            return prod.save()
            .then(result =>{
                console.log(`${result.prodName} has been added to cart`)
                return true;
            })
        }else{
            console.log(`out of stock`);
            return false;
        }
    })
    .catch(err=>console.log(err))
    console.log(isProdUpdated);
    (isUserUpdated == true && isProdUpdated == true)? res.send(`${receiptData.prodName} has been added to cart`) : res.send('out of stock!');
}

module.exports.finalCheckout = async (req, res) =>{
    const userData = auth.decode(req.headers.authorization);
    let receiptData = {
        userId: userData.id,
        email: userData.email
    }
    await users.findById(receiptData.userId)
    .then(result => {
        console.log(`checking out items:\n ${result.orders}`)
        result.orders.splice(0, result.orders.length);
        console.log(`please proceed to payment`)
        console.log(result)
        result.save();
        res.send(`please proceed to payment`)
    })
    .catch(err=>console.log(err))
}

//? set user to admin
module.exports.setUserToAdmin = async (req, res) =>{
    const adminCheck = await auth.decode(req.headers.authorization).isAdmin;
    if(adminCheck == true){
        users.findByIdAndUpdate(req.params.userId, {
            isAdmin: true},
            {new: true}
        )
        .then(
            res.send(`user has been promoted to admin!`))
        .catch(err=>res.send(err))
    }else{
        console.log(`admin auth failed`)
        res.send(`User doesn't have admin privileges`)
    }
}

//? retrieve authenticated user's orders
module.exports.retrieveUserOrder = async (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    users.findById(userData.id)
    .then(result => {
        if(result.orders.length>0){
            let orderTotal = 0;
            let itemsInCart = 0;
            result.orders.forEach(sumOrders => {
                orderTotal += sumOrders.orderPrice;
                itemsInCart ++;
            })
            console.log(orderTotal)
            res.send(`Number of items in cart: ${itemsInCart}\nOrder total amount: ${orderTotal}`)
        }else{
            res.send(`You haven't placed an order yet!`)
        }
    })
    .catch(err=> res.send(err))
}

module.exports.retrieveUserOrder2 = async (req, res) =>{
    const userData = auth.decode(req.headers.authorization);
    users.findById(userData.id)
    .then(receiptHolder => {
        if(receiptHolder.orders.length > 0){
            let perProductName = receiptHolder.orders.map(element => element.productName)
            let distinctProductName = [...new Set(perProductName)]
            let perProductPrice = receiptHolder.orders.map(element=> element.orderPrice)
            let totalPrice = 0;
            let concatReceipt = "";
            distinctProductName.forEach(prodIndexName => {
                let distinctProductCounter = 0;
                let distinctProductPriceCounter = 0;
                for(let i = 0; i < perProductName.length; i++){
                    if(perProductName[i] == prodIndexName){
                        distinctProductCounter ++;
                        distinctProductPriceCounter += perProductPrice[i];                    
                    }
                }
                console.log(distinctProductPriceCounter)
                totalPrice += distinctProductPriceCounter;
                concatReceipt += prodIndexName + " : " + distinctProductCounter +  " - " + "₱" + distinctProductPriceCounter + "\n<br>"
            })
            console.log(`${concatReceipt} ${totalPrice}`)
            res.send(`${concatReceipt}\n<b class="mt-4">TOTAL PRICE: ₱${totalPrice}</b>`)
        }else{
            res.send(`You haven't placed an order yet! `)
        }
    })
}

module.exports.deleteUser = (req, res) =>{
    const adminCheck = auth.decode(req.headers.authorization).isAdmin;
    if(adminCheck){
        users.findByIdAndDelete(req.params.userId)
        .then(res.send(`user has been deleted successfully!`))
        .catch(err => res.send(err));
    }else{
        res.send(`User is not authorized`)
    }
}

module.exports.removeAdmin = async (req, res) =>{
    const adminCheck = await auth.decode(req.headers.authorization).isAdmin;
    if(adminCheck == true){
        users.findByIdAndUpdate(req.params.userId, {
            isAdmin: false},
            {new: true}
        )
        .then(
            res.send(`admin status has been removed!`))
        .catch(err=>res.send(err))
    }else{
        console.log(`admin auth failed`)
        res.send(`User doesn't have admin privileges`)
    }
}

module.exports.getAllUsers = (req, res) =>{
    users.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error));
}